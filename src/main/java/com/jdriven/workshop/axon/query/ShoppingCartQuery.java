package com.jdriven.workshop.axon.query;

import lombok.Value;

@Value
public class ShoppingCartQuery {
    String productId;
}
